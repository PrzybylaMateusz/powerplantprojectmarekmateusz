﻿using System;
using System.Collections.Generic;

namespace PowerPlant.Alerts.Dates.Models
{
    internal class ListOfAlertsInHour
    {
        public DateTime StartOfPeriod;
        public DateTime EndOfPeriod;
        public int NumberOfAlerts;
    }
}