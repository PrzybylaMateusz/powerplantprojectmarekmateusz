﻿using System;
using System.Collections.Generic;

namespace PowerPlant.Alerts.Dates.Models
{
    internal class AlertData
    {
        public DateTime StartDate;
        public DateTime EndDate;
        public List<OutOfRange> ListOfAlerts;
    }
}
