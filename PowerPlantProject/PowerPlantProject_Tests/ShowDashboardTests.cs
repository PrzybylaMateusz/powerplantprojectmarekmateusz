﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerPlantDataProvider;
using PowerPlantProject;

namespace PowerPlantProject_Tests
{
    [TestClass]
    public class ShowDashboardTests
    {
        [TestMethod]
        public void OutOfRangeParameters_ProvideWrongParameters_CheckConsoleColor()
        {
            var plant = PowerPlant.Instance;
            var cauldron = plant.Cauldrons[0];

            double waterPressureOverMax = 14;
            double waterPressureUnderMin = 10;

            var showDashboard = new ShowDashboard();

            showDashboard.CheckIsOutOfRange(cauldron.WaterPressure.MinValue, cauldron.WaterPressure.MaxValue,
                cauldron.WaterPressure.TypicalValue, cauldron.WaterPressure.Unit, waterPressureOverMax);

            Assert.IsTrue(Console.ForegroundColor == ConsoleColor.Red);

            Console.ResetColor();

            showDashboard.CheckIsOutOfRange(cauldron.WaterPressure.MinValue, cauldron.WaterPressure.MaxValue,
                cauldron.WaterPressure.TypicalValue, cauldron.WaterPressure.Unit, waterPressureUnderMin);

            Assert.IsTrue(Console.ForegroundColor == ConsoleColor.Red);
        }

        [TestMethod]
        public void OutOfRangeParameters_ProvideValidParameters_CheckConsoleColor()
        {
            var plant = PowerPlant.Instance;
            var cauldron = plant.Cauldrons[0];

            double waterPressureTypicalVal = 12;

            var showDashboard = new ShowDashboard();

            showDashboard.CheckIsOutOfRange(cauldron.WaterPressure.MinValue, cauldron.WaterPressure.MaxValue,
                cauldron.WaterPressure.TypicalValue, cauldron.WaterPressure.Unit, waterPressureTypicalVal);

            Assert.IsTrue(Console.ForegroundColor != ConsoleColor.Red);
        }
    }
}
