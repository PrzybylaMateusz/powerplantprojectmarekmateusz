﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PowerPlantProject;

namespace PowerPlantProject_Tests
{
    [TestClass]
    public class CalculateEnergyTests
    {
        [TestMethod]
        public void EnergyGeneratedCalculation_ProvideValues_CheckIsCalculationCorrect()
        {
            var calculationClass = new CalculatedOfGeneratedEnergy();
            var currentPower = 100;
            var timeInHours = 3;

            var result = calculationClass.Calculate(currentPower, timeInHours);
            double exceptedResult = 300;

            Assert.AreEqual(result, exceptedResult);
        }
    }
}
