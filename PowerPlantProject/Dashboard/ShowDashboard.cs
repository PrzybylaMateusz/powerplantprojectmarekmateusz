﻿using System;
using System.Threading;
using PowerPlantDataProvider;
using PowerPlantProject;

namespace Dashboard
{
    class ShowDashboard
    {
        

        public void ShowDash()
        {
            var plant = PowerPlant.Instance;

            while (true)
            {
                Console.Clear();
                Console.WriteLine(plant.Name);

                foreach (var cauldron in plant.Cauldrons)
                {
                    Console.WriteLine($"\t{cauldron.Name}");
                    PrintWhite("Camber Temp ------------ ");
                    CheckIsOutOfRange(cauldron.CamberTemperature);
                    PrintUnits(cauldron.CamberTemperature.Unit);
                    PrintWhite("Water Pressure --------- ");
                    CheckIsOutOfRange(cauldron.WaterPressure);
                    PrintUnits(cauldron.WaterPressure.Unit);
                    PrintWhite("Water Temp ------------- ");
                    CheckIsOutOfRange(cauldron.WaterTemperature);
                    PrintUnits(cauldron.WaterTemperature.Unit);
                }
                foreach (var trafo in plant.Transformators)
                {
                    
                    Console.WriteLine($"\t{trafo.Name}");
                    PrintWhite("Input Voltage ---------- ");
                    CheckIsOutOfRange(trafo.InputVoltage);
                    PrintUnits(trafo.InputVoltage.Unit);
                    PrintWhite("Output Voltage --------- ");
                    CheckIsOutOfRange(trafo.OutputVoltage);
                    PrintUnits(trafo.OutputVoltage.Unit);
                }
                foreach (var turbine in plant.Turbines)
                {
                    Console.WriteLine($"\t{turbine.Name}");
                    PrintWhite("Current Power ---------- ");
                    CheckIsOutOfRange(turbine.CurrentPower);
                    PrintUnits(turbine.CurrentPower.Unit);
                    PrintWhite("Output Voltage --------- ");
                    CheckIsOutOfRange(turbine.OutputVoltage);
                    PrintUnits(turbine.OutputVoltage.Unit);
                    PrintWhite("Overheater Steam Temp -- ");
                    CheckIsOutOfRange(turbine.OverheaterSteamTemperature);
                    PrintUnits(turbine.OverheaterSteamTemperature.Unit);
                    PrintWhite("Rotation Speed --------- ");
                    CheckIsOutOfRange(turbine.RotationSpeed);
                    PrintUnits(turbine.RotationSpeed.Unit);
                    PrintWhite("Steam Pressure --------- ");
                    CheckIsOutOfRange(turbine.SteamPressure);
                    PrintUnits(turbine.SteamPressure.Unit);
                }

                Thread.Sleep(250);
            }
        }

        private void PrintUnits(string unit)
        {
            Console.ResetColor();
            Console.Write($" {unit}\n");
        }

        private void PrintWhite(string message)
        {
            Console.ResetColor();
            Console.Write(message);
        }

        private void CheckIsOutOfRange(AssetParameter asset)
        {
                if (asset.CurrentValue > asset.MaxValue || asset.CurrentValue < asset.MinValue)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else
                {
                    Console.ResetColor();
                }
            
           

            Console.Write($"{asset.CurrentValue:0.000}");
        }
    }
}
