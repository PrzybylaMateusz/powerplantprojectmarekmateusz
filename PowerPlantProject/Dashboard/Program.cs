﻿using System;
using System.Threading;
using PowerPlantProject.Models;

namespace Dashboard
{
    class Program
    {
        static void Main()
        {
            var t = new Thread(Run);
            t.Start();
            Exit();
        }

        static void Run()
        {
            new ShowDashboard().ShowDash();
        }

        static void Exit()
        {
            if (Console.ReadKey().Key == ConsoleKey.Escape)
            {
                Environment.Exit(0);
            }
        }
    }
}
