﻿namespace PowerPlantProject.Models
{
    public class ConfigurationFile
    {
        public int PermissibleDeviation;
        public int FrequencyOfRefreshing;
    }
}