﻿using System;
using System.Collections.Generic;

namespace PowerPlantProject.Models
{
    internal class AlertData
    {
        public DateTime StartDate;
        public DateTime EndDate;
        public List<OutOfRange> ListOfAlerts;
    }
}
