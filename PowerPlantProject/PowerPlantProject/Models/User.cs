﻿using System.ComponentModel.DataAnnotations;

namespace PowerPlantProject.Models
{
    public class User
    {
        [Key]
        public string Name { get; set; }
        public string Password { get; set; }
        public string Permissions { get; set; }
    }
}