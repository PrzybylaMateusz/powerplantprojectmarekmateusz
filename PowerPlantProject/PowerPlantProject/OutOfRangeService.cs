﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using PowerPlantDataProvider;
using PowerPlantProject.Models;
using PowerPlantProject.Repositories;

namespace PowerPlantProject
{
    internal class OutOfRangeService
    {
        private static PowerPlant _plant;
        private static Dictionary<int, string> _deviceDictionary = new Dictionary<int, string>();
        private static bool _ifAdded = true;
        private static bool _ifExists = false;
        private static double _minValue;
        private static double _maxValue;
        private static int _permisibleDeviation;
        private static int _refreshFreq;
        private static int _counter;
        
        public OutOfRangeService(PowerPlant plant)
        {
            _plant = plant;
            var t = new Thread(BackgroundMonitoring);
            t.Start();
        }

        public static void BackgroundMonitoring()
        {
            if (ConfigurationJsonFile.CheckAndUseConfigurationFile() != null)
            {
                ChangeDefaultValues();
            }

            while (true)
            {
                foreach (var cauldron in _plant.Cauldrons)
                {
                    GetPropertyValue(cauldron.Name, cauldron);
                }
                foreach (var trafo in _plant.Transformators)
                {
                    GetPropertyValue(trafo.Name, trafo);
                }
                foreach (var turbine in _plant.Turbines)
                {
                    GetPropertyValue(turbine.Name, turbine);
                }

                Thread.Sleep(_ifExists == false ? 250 : _refreshFreq);
            }
        }

        private static void GetPropertyValue<T>(string deviceName ,T obj)
        {
            foreach (var propertyValue in obj.GetType().GetProperties())
            {
                var value = propertyValue.GetValue(obj);
                var asset = value as AssetParameter;
                if (asset == null)
                    continue;
                if (_ifExists == false)
                {
                    _minValue = asset.MinValue;
                    _maxValue = asset.MaxValue;
                }
                else
                {
                    _minValue = asset.TypicalValue - (asset.TypicalValue * _permisibleDeviation)/100;
                    _maxValue = asset.TypicalValue + (asset.TypicalValue * _permisibleDeviation)/100;
                }
                Check(deviceName, propertyValue.Name, asset);
            }
        }

        private static void Check(string deviceName, string parameterName, AssetParameter asset)
        {
            _counter++;

            if (asset.CurrentValue > _maxValue || asset.CurrentValue < _minValue)
            {
                _ifAdded = _deviceDictionary.ContainsValue(parameterName + deviceName);

                if (_ifAdded == false)
                {
                    _deviceDictionary.Add(_counter, parameterName + deviceName);
                    SendToDb(deviceName, parameterName);
                }
            }

            if (!(asset.CurrentValue < _maxValue) || !(asset.CurrentValue > _minValue) ||
                !_deviceDictionary.ContainsValue(parameterName + deviceName)) return;
            var a = _deviceDictionary.SingleOrDefault(d => d.Value == (parameterName + deviceName));
            _deviceDictionary.Remove(a.Key);
        }

        private static void SendToDb(string deviceName, string parameterName)
        {
            var outOfRangeRepo = new OutOfRangeRepository();
            var outOfRange = new OutOfRange
            {
                DeviceName = deviceName,
                ParameterName = parameterName,
                DateTime = DateTime.Now,
                LoggedUser = ProgramLoop.ActualUser == null ? "N/A" : ProgramLoop.ActualUser.Name
            };
            outOfRangeRepo.AddRecord(outOfRange);
        }

        public static void ChangeDefaultValues()
        {
            _ifExists = true;
            _permisibleDeviation = ConfigurationJsonFile.CheckAndUseConfigurationFile().PermissibleDeviation;
            _refreshFreq = ConfigurationJsonFile.CheckAndUseConfigurationFile().FrequencyOfRefreshing;
        }
    }
}