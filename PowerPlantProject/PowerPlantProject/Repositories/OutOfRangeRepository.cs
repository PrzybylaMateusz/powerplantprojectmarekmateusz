﻿using System;
using System.Collections.Generic;
using System.Linq;
using PowerPlantProject.DbContext;
using PowerPlantProject.Models;

namespace PowerPlantProject.Repositories
{
    internal class OutOfRangeRepository
    {
        public void AddRecord(OutOfRange outOfRange)
        {
            using (var dbContext = new PowerPlantDbContext())
            {
                dbContext.OutOfRangeDbSet.Add(outOfRange);
                dbContext.SaveChanges();
            }
        }

        public List<OutOfRange> GetAllRecordsInSelectedPeriod(DateTime startDate, DateTime endDate)
        {
            List<OutOfRange> listsOfAllAlerts;
            using (var dbContext = new PowerPlantDbContext())
            {
                listsOfAllAlerts = dbContext.OutOfRangeDbSet.Where(sd => sd.DateTime > startDate && sd.DateTime < endDate ).ToList();
                dbContext.SaveChanges();
            }
            return listsOfAllAlerts;
        }
    }
}