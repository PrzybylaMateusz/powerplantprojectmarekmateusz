﻿using System.Collections.Generic;
using System.Linq;
using PowerPlantProject.DbContext;
using PowerPlantProject.Models;

namespace PowerPlantProject.Repositories
{
    internal class UserRepository
    {
        public void AddUser(User user)
        {
           using (var dbContext = new PowerPlantDbContext())
            {
                dbContext.UserDbSet.Add(user);
                dbContext.SaveChanges();
            }
        }

        public List<User> GetAllUsers()
        {
            List<User> listOfAllUsers;
            using (var dbContext = new PowerPlantDbContext())
            {
                listOfAllUsers = dbContext.UserDbSet.ToList();
            }
            return listOfAllUsers;
        }

        public void RemoveUser(User userToRemove)
        {
            using (var dbContext = new PowerPlantDbContext())
            {
                dbContext.UserDbSet.Attach(userToRemove);
                dbContext.UserDbSet.Remove(userToRemove);
                dbContext.SaveChanges();
            }
        }
    }
}