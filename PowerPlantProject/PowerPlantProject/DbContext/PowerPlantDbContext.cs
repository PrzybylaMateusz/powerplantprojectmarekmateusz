﻿using System.Configuration;
using System.Data.Entity;
using PowerPlantProject.Models;

namespace PowerPlantProject.DbContext
{
    public class PowerPlantDbContext : System.Data.Entity.DbContext
    {
        public PowerPlantDbContext() : base(GetConnectionString())
        { }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["PowerPlantDbMarekMateusz"].ConnectionString;
        }

        public DbSet<User> UserDbSet { get; set; }
        public DbSet<OutOfRange> OutOfRangeDbSet { get; set; }
    }
}