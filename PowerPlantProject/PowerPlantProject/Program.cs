﻿using PowerPlantDataProvider;

namespace PowerPlantProject
{
    internal class Program
    {
        private static void Main()
        {
            new OutOfRangeService(PowerPlant.Instance);
            ProgramLoop.Run();
        }
    }
}