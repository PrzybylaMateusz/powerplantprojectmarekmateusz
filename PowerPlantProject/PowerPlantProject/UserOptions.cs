﻿using System;
using System.Linq;
using PowerPlantProject.Models;
using PowerPlantProject.Repositories;

namespace PowerPlantProject
{
    internal class UserOptions
    {
        public User LoginPanel()
        {
            var userRepository = new UserRepository();
            if (userRepository.GetAllUsers().Count == 0)
            {
                Console.WriteLine("No user added. Add new user");
                AddNewUser(new User());
            }
            string inputName = null;
            var exit = false;
            while (!exit)
            {
                Console.WriteLine("Log in: ");
                Console.Write("Enter user name: ");
                inputName = Console.ReadLine();
                if (!userRepository.GetAllUsers().Select(n => n.Name).Contains(inputName))
                {
                    Console.WriteLine("User with given name doesn't exist");
                }
                else
                {
                    exit = true;
                }
            }
            while (exit)
            {
                Console.Write("Enter password: ");
                var inputPassword = Console.ReadLine();
                var actualUser = userRepository.GetAllUsers().First(n => n.Name == inputName);
                if (actualUser.Password != inputPassword)
                {
                    Console.WriteLine("Wrong password! Try again!");
                }
                else
                {
                    exit = false;
                }
            }
            return userRepository.GetAllUsers().Find(n => n.Name == inputName);
        }

        public void AddNewUser(User actualUser)
        {
            if (actualUser.Permissions == "user")
            {
                Console.WriteLine("Access denied. Required admin permissions");
            }
            else
            {
                var user = new User();

                Console.Write("Enter new user name: ");
                user.Name = IdiotResistance.ProvideValidName();
                Console.Write("Enter password:");
                user.Password = Console.ReadLine();
                Console.WriteLine("Add permissions (admin/user)");
                user.Permissions = IdiotResistance.ProvideValidPermissions();

                var userRepository = new UserRepository();
                userRepository.AddUser(user);
            }
        }

        public void RemoveUser(User actualUser)
        {
            var userRepository = new UserRepository();
            if (actualUser.Permissions == "user")
            {
                Console.WriteLine("Access denied. Required admin permissions");
            }
            else
            {
                var exit = false;
                while (!exit)
                {
                    Console.WriteLine("Enter password");
                    var inputPassword = Console.ReadLine();
                    if (inputPassword != actualUser.Password)
                    {
                        Console.WriteLine("Access denied. Wrong password. Try again!");
                    }
                    else
                    {
                        exit = true;
                    }
                }
                Console.WriteLine("Choose user to remove:");
                foreach (var userName in userRepository.GetAllUsers().Select(n=>n.Name))
                {
                    Console.WriteLine("- " +userName);
                }
                var userNameToRemove = Console.ReadLine();
                if (!(userRepository.GetAllUsers().Select(n => n.Name).Contains(userNameToRemove)))
                {
                    Console.WriteLine("User with the given name doesnt exist. Try again!");
                }
                else if (userNameToRemove == actualUser.Name)
                {
                    Console.WriteLine("You can not remove yourself!");
                }
                else
                {
                    var userToRemove = userRepository.GetAllUsers().Find(n => n.Name == userNameToRemove);
                    userRepository.RemoveUser(userToRemove);
                }
            }
        }
    }
}