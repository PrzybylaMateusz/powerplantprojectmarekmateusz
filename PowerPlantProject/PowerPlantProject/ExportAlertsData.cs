﻿using System;
using System.IO;
using Newtonsoft.Json;
using PowerPlantProject.Models;
using PowerPlantProject.Repositories;
using Formatting = System.Xml.Formatting;

namespace PowerPlantProject
{
    internal class ExportAlertsData
    {
        public static void ProvideAlertInfo()
        {
            var alertData = new AlertData();
            var outOfRangeRepository = new OutOfRangeRepository();

            Console.WriteLine("Provide start date (mm/dd/yyyy hh:mm): ");
            alertData.StartDate = IdiotResistance.ProvideValidStartDate();
            Console.WriteLine("Provide end date (mm/dd/yyyy hh:mm): ");
            alertData.EndDate = IdiotResistance.ProvideValidEndDate(alertData.StartDate);

            alertData.ListOfAlerts = outOfRangeRepository.GetAllRecordsInSelectedPeriod(alertData.StartDate, alertData.EndDate);

            SaveAlertsDataToJsonFile(alertData);
        }

        private static void SaveAlertsDataToJsonFile(AlertData alertData)
        {
            Console.WriteLine("Enter filename: ");
            string userInput = null;
            var exit = false;
            while (!exit)
            {
                userInput = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(userInput))
                {
                    Console.WriteLine("Field can not be empty! Ttry again");
                }
                else
                {
                    exit = true;
                }
            }
            var enter = userInput + ".json";
            var filepath = @enter;
            File.WriteAllText(filepath, JsonConvert.SerializeObject(alertData, (Newtonsoft.Json.Formatting) Formatting.Indented));
        }
    }
}