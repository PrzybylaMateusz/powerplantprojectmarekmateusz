﻿using System;
using System.IO;
using Newtonsoft.Json;
using PowerPlantProject.Models;
using Formatting = System.Xml.Formatting;

namespace PowerPlantProject
{
    internal class ConfigurationJsonFile
    {
        public static void CheckPermissions(string permission)
        {
            if (permission == "admin")
            {
                CreateConfigurationJsonFile();
            }
            else
            {
                Console.WriteLine("Access denied.Required admin permissions");
            }
        }

        private static void CreateConfigurationJsonFile()
        {
            var configurationFileData =
                new ConfigurationFile();
            Console.WriteLine("Provide permissible deviation (%): ");
            configurationFileData.PermissibleDeviation = IdiotResistance.ProvideValidConfigurationFileData();
            Console.WriteLine("Provide frequency of refreshing (ms): ");
            configurationFileData.FrequencyOfRefreshing = IdiotResistance.ProvideValidConfigurationFileData();
            SaveReportDataToFile(configurationFileData);
        }

        public static void SaveReportDataToFile(ConfigurationFile configurationFile)
        {

            const string enter = "configurationFile.json";
            const string filepath = @enter;
            File.WriteAllText(filepath, JsonConvert.SerializeObject(configurationFile, (Newtonsoft.Json.Formatting)Formatting.Indented));
            OutOfRangeService.ChangeDefaultValues();
        }

        public static ConfigurationFile CheckAndUseConfigurationFile()
        {
            ConfigurationFile configurationFile;
            var a = Directory.GetFiles(Directory.GetCurrentDirectory(), "configurationFile.json");
            if (a.Length != 0)
            {
                var readContent = File.ReadAllText(a[0]);
                configurationFile = JsonConvert.DeserializeObject<ConfigurationFile>(readContent);
            }
            else
            {
                configurationFile = null;
            }
            return configurationFile;
        }
    }
}