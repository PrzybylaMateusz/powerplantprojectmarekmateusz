﻿using System;
using PowerPlantProject.Models;

namespace PowerPlantProject
{
    public class ProgramLoop
    {
        public static ConfigurationFile ConfigurationFileData;
        public static User ActualUser;

        public static void Run()
        {
            var login = new UserOptions();
            ActualUser = login.LoginPanel();
            ConfigurationFileData = ConfigurationJsonFile.CheckAndUseConfigurationFile();

            while (true)
            {
                Console.WriteLine("\nChoose option: ");
                Console.WriteLine("<1> Add new user");
                Console.WriteLine("<2> Remove user");
                Console.WriteLine("<3> Run Dashboard");
                Console.WriteLine("<4> Show actual and generated energy");
                Console.WriteLine("<5> Create or edit configuration file");
                Console.WriteLine("<6> Export alerts data to file");
                Console.WriteLine("<Q> Exit");

                var choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        login.AddNewUser(ActualUser);
                        break;
                    case "2":
                        login.RemoveUser(ActualUser);
                        break;
                    case "3":
                        new ShowDashboard().StartShowingDash();
                        break;
                    case "4":
                        new CalculatedOfGeneratedEnergy().StartActualAndGeneratedEnergy();
                        break;
                    case "5":
                        ConfigurationJsonFile.CheckPermissions(ActualUser.Permissions);
                        break;
                    case "6":
                        ExportAlertsData.ProvideAlertInfo();
                        break;
                    case "Q":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Wrong command. Try again");
                        break;
                }
            }
        }
    }
}