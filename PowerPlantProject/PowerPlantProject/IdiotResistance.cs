﻿using System;
using System.Linq;
using PowerPlantProject.Repositories;

namespace PowerPlantProject
{
    internal class IdiotResistance
    {
        public static string ProvideValidName()
        {
            string userInput = null;
            var exit = false;
            while (!exit)
            {
                userInput = Console.ReadLine();
                var userRepository = new UserRepository();

                if (string.IsNullOrWhiteSpace(userInput))
                {
                    Console.WriteLine("Field can not be empty. Try again!");
                }
                else if (userRepository.GetAllUsers().Select(n => n.Name).Contains(userInput))
                {
                    Console.WriteLine("Login already exists");
                }
                else
                {
                    exit = true;
                }
            }
            return userInput;
        }

        public static string ProvideValidPermissions()
        {
            string userInput = null;
            var exit = false;
            var userRepository = new UserRepository();
            while (!exit)
            {
                userInput = Console.ReadLine();

                if (userInput != "admin" && userInput != "user")
                {
                    Console.WriteLine("Incorect option. Choose admin or user. Try again!");
                }
                if (userInput == "user" &&
                    userRepository.GetAllUsers().SingleOrDefault(u => u.Permissions == "admin") == null)
                {
                    Console.WriteLine("First user must be admin (if you want add new users)");
                }
                else
                {
                    exit = true;
                }
            }
            return userInput;
        }

        public static int ProvideValidConfigurationFileData()
        {
            var userInput = 0;
            var exit = false;
            while (!exit)
            {
                while (!int.TryParse(Console.ReadLine(), out userInput))
                {
                    Console.WriteLine("Value must be the number! Try again!");
                }
                if (userInput <= 0)
                {
                    Console.WriteLine("Value can not be less than 0");
                }
                else
                {
                    exit = true;
                }
            }
            return userInput;
        }

        public static DateTime ProvideValidStartDate()
        {
            var userInput = new DateTime();

            var exit = false;
            while (!exit)
            {
                while (!DateTime.TryParse(Console.ReadLine(), out userInput))
                {
                    Console.WriteLine("Wrong date format - try again! (mm/dd/yyyy hh:mm)");
                }

                if (userInput > DateTime.Now)
                {
                    Console.WriteLine(
                        $"No revenue for the selected start date: {userInput}. You must provide date before now!");
                }
                else
                {
                    exit = true;
                }
            }
            return userInput;
        }

        public static DateTime ProvideValidEndDate(DateTime startDate)
        {
            var userInput = new DateTime();
            var exit = false;
            while (!exit)
            {
                while (!DateTime.TryParse(Console.ReadLine(), out userInput))
                {
                    Console.WriteLine("Wrong date format - try again! (dd/mm/yyyy hh:mm)");
                }
                if (userInput < startDate)
                {
                    Console.WriteLine("End date can't be earlier than the start date. Try again!");
                }
                else
                {
                    exit = true;
                }
            }
            return userInput;
        }
    }
}