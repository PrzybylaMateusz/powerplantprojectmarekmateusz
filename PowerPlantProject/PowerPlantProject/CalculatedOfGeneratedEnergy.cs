﻿using System;
using System.Threading;
using PowerPlantDataProvider;

namespace PowerPlantProject
{
    public class CalculatedOfGeneratedEnergy
    {
        public void StartActualAndGeneratedEnergy()
        {
            var thread = new Thread(ShowActualAndGeneratedEnergy);
            thread.Start();
            if (Console.ReadKey().Key == ConsoleKey.Escape)
            {
                thread.Abort();
            }
        }

        public void ShowActualAndGeneratedEnergy()
        {
            var configurationFile = ProgramLoop.ConfigurationFileData;
            var actualTime = 250;
            if (configurationFile != null)
            {

                var actualConfigurationFile = ConfigurationJsonFile.CheckAndUseConfigurationFile();
                actualTime = actualConfigurationFile.FrequencyOfRefreshing;
            }

            var plant = PowerPlant.Instance;
            var startValue = 0.0;
            var time = TimeSpan.FromMilliseconds(actualTime);
            var timeInHours = time.TotalHours;

            while (true)
            {
                Console.Clear();
                foreach (var turbine in plant.Turbines)
                {
                    var currentPower = turbine.CurrentPower.CurrentValue;
                    var pad = '-';
                    //Console.Write($"{message.PadRight(30, pad)} ");
                    Console.WriteLine($"\t{turbine.Name}");
                    Console.Write(
                        $"Current Power".PadRight(30, pad));
                    Console.WriteLine($"{ currentPower} { turbine.CurrentPower.Unit}");
                    Console.Write(
                        $"Generated Power".PadRight(30, pad));
                    Console.WriteLine(
                        $"{startValue += Calculate(currentPower, timeInHours)} {turbine.CurrentPower.Unit}");
                }
                Thread.Sleep(actualTime);
            }
        }

        public double Calculate(double currentPower, double timeInHours)
        {
            return currentPower * timeInHours;
        }
    }
}