﻿using System;
using System.Threading;
using PowerPlantDataProvider;

namespace PowerPlantProject
{

    public class ShowDashboard
    {
        public void StartShowingDash()
        {
            var thread = new Thread(ShowDashReflection);
            thread.Start();
            if (Console.ReadKey().Key == ConsoleKey.Escape)
            {
                thread.Abort();
            }
        }

        public void ShowDashReflection()
        {
            var plant = PowerPlant.Instance;
            var configurationFile = ProgramLoop.ConfigurationFileData;

            while (true)
            {
                Console.Clear();
                Console.WriteLine(plant.Name);

                foreach (var cauldron in plant.Cauldrons)
                {
                    Console.WriteLine($"\t{cauldron.Name}");
                    GetPropertyValue(cauldron);
                }
                foreach (var trafo in plant.Transformators)
                {
                    Console.WriteLine($"\t{trafo.Name}");
                    GetPropertyValue(trafo);
                }
                foreach (var turbine in plant.Turbines)
                {
                    Console.WriteLine($"\t{turbine.Name}");
                    GetPropertyValue(turbine);
                }
                if (configurationFile == null)
                {
                    Thread.Sleep(250);
                }
                else
                {
                    var actualConfigurationFile = ConfigurationJsonFile.CheckAndUseConfigurationFile();
                    Thread.Sleep(actualConfigurationFile.FrequencyOfRefreshing);
                }
            }
        }

        public void PrintUnits(string unit)
        {
            Console.ResetColor();
            Console.Write($" {unit}\n");
        }

        public void PrintWhite(string message)
        {
            Console.ResetColor();
            var pad = '-';
            Console.Write($"{message.PadRight(30, pad)} ");
        }

        private void GetPropertyValue<T>(T obj)
        {
            foreach (var propertyValue in obj.GetType().GetProperties())
            {
                var value = propertyValue.GetValue(obj);
                var asset = value as AssetParameter;
                if (asset == null)
                    continue;

                PrintWhite($"{propertyValue.Name} ");
                CheckIsOutOfRange(asset.MinValue, asset.MaxValue, asset.TypicalValue, asset.Unit, asset.CurrentValue);
                PrintUnits(asset.Unit);
            }
        }

        public void CheckIsOutOfRange(double minVal, double maxVal, double typicalVal, string unit, double currentVal)
        {
            var configurationFile = ProgramLoop.ConfigurationFileData;
            if (configurationFile == null)
            {
                if (currentVal > maxVal || currentVal < minVal)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else
                {
                    Console.ResetColor();
                }
            }
            else
            {
                var actualConfigurationFile = ConfigurationJsonFile.CheckAndUseConfigurationFile();
                if (currentVal > (typicalVal + typicalVal * actualConfigurationFile.PermissibleDeviation / 100)
                    || currentVal < (typicalVal - typicalVal * actualConfigurationFile.PermissibleDeviation / 100))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }
                else
                {
                    Console.ResetColor();
                }
            }
            Console.Write($"{currentVal:0.000}");
        }
    }
}