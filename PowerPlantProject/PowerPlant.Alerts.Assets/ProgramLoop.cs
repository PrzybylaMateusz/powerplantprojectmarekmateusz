﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace PowerPlant.Alerts.Assets
{
    internal class ProgramLoop
    {
        public static List<string> ListOfFilepaths;
        public static List<string> ListOfFileNames = new List<string>();

        public static void Run()
        {
            while (true)
            {
                ShowAlertsFiles();
                var selectedFile = GetSelectedFile();
                ShowAlerts(selectedFile);
            }
        }

        private static void ShowAlerts(AlertData selectedFile)
        {
            var alertsData = selectedFile;
            Console.WriteLine("\n\nRange of date: " + alertsData.StartDate + " - " + alertsData.EndDate + "\n\n");

            if (alertsData.ListOfAlerts == null || alertsData.ListOfAlerts.Count == 0)
            {
                Console.WriteLine("No alerts !\n\n");
                return;
            }

            var listOfAlertsForDevice = new List<ListOfAlerts>();

            Console.WriteLine("Name of device:         Numbers of alerts:\n\n");
            foreach (var alert in alertsData.ListOfAlerts)
            {
                var check =
                    listOfAlertsForDevice.SingleOrDefault(a => a.NameOfDevice == alert.DeviceName);

                if (check != null)
                {
                    check.NumberOfAlerts += 1;
                }

                else
                {
                    var newAlert = new ListOfAlerts
                    {
                        NameOfDevice = alert.DeviceName,
                        NumberOfAlerts = 1
                    };
                    listOfAlertsForDevice.Add(newAlert);
                }
            }

            foreach (var position in listOfAlertsForDevice)
            {
                Console.WriteLine(position.NameOfDevice + "\t\t" + position.NumberOfAlerts);
            }
            Console.WriteLine("\n\n");
        }

        private static AlertData GetSelectedFile()
        {
            string userInput = null;
            var exit = false;
            while (!exit)
            {
                userInput = Console.ReadLine();
                if (!ListOfFileNames.Contains(userInput + ".json"))
                {
                    Console.WriteLine("No file with given name. Try again!");
                }
                else
                {
                    exit = true;
                }
            }

            var readContent = File.ReadAllText(
                Path.GetFullPath(@"..\..\..\PowerPlantProject\bin\Debug\" + userInput + ".json"));

            var alertsData = JsonConvert.DeserializeObject<AlertData>(readContent);
            return alertsData;
        }

        private static void ShowAlertsFiles()
        {
            Console.WriteLine("Select a file from the list below(enter name):\n\n");

            ListOfFilepaths = Directory.GetFiles(@"..\..\..\PowerPlantProject\bin\debug", "*.json").ToList();
            ListOfFilepaths.Remove(@"..\..\..\PowerPlantProject\bin\debug\configurationFile.json");

            if (ListOfFilepaths.Count == 0)
            {
                Console.WriteLine("No files with alerts data");
            }
            foreach (var filePath in ListOfFilepaths)
            {
                var fileName = Path.GetFileName(filePath);
                if (fileName != null)
                    Console.WriteLine(fileName.Remove(fileName.Length - 5));
                ListOfFileNames.Add(fileName);
            }
        }
    }
}