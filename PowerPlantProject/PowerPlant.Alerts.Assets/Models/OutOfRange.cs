﻿using System;

namespace PowerPlant.Alerts.Assets
{
    public class OutOfRange
    {
        public int Id { get; set; }
        public string DeviceName { get; set; }
        public string ParameterName { get; set; }
        public DateTime DateTime { get; set; }
        public string LoggedUser { get; set; }
    }
}
